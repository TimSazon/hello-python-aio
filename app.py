from aiohttp import web
from aiohttp_wsgi import WSGIHandler
from bottle import Bottle
import socket


app = Bottle(__name__)


@app.route('/')
def hello():
    html = "<h3>Hello World!</h3>" \
           "<b>Hostname:</b> {hostname}"
    return html.format(hostname=socket.gethostname())


def make_aiohttp_app(app):
    wsgi_handler = WSGIHandler(app)
    aioapp = web.Application()
    aioapp.router.add_route('*', '/{path_info:.*}', wsgi_handler)
    return aioapp


aioapp = make_aiohttp_app(app)
