FROM python:3.5.4-alpine

ADD app.py /

RUN pip install bottle aiohttp aiohttp_wsgi gunicorn

EXPOSE 8000

CMD gunicorn app:aioapp -k aiohttp.worker.GunicornWebWorker -w 9 -b 0.0.0.0:8000